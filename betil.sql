-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 27, 2015 at 08:16 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `betil`
--

-- --------------------------------------------------------

--
-- Table structure for table `auto`
--

CREATE TABLE IF NOT EXISTS `auto` (
`id` int(11) NOT NULL,
  `aktivan` tinyint(1) NOT NULL,
  `marka` int(11) NOT NULL,
  `model_dodatno` varchar(200) NOT NULL,
  `vrsta_auta` varchar(20) NOT NULL,
  `karoserija` varchar(25) NOT NULL,
  `vrata` varchar(3) NOT NULL,
  `sjedala` varchar(2) NOT NULL,
  `boja` varchar(30) NOT NULL,
  `mjesec_dodan` varchar(2) NOT NULL,
  `godina` int(11) NOT NULL,
  `registracijaMjesec` varchar(2) NOT NULL,
  `registracijaGodina` int(11) NOT NULL,
  `kilometri` int(11) NOT NULL,
  `motor` varchar(15) NOT NULL,
  `zapremnina` int(11) NOT NULL,
  `snaga` int(11) NOT NULL,
  `mjenjac` varchar(20) NOT NULL,
  `pogon` varchar(15) NOT NULL,
  `cijena` int(11) NOT NULL,
  `opis` varchar(1500) NOT NULL,
  `dodan` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uredjen` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auto`
--

INSERT INTO `auto` (`id`, `aktivan`, `marka`, `model_dodatno`, `vrsta_auta`, `karoserija`, `vrata`, `sjedala`, `boja`, `mjesec_dodan`, `godina`, `registracijaMjesec`, `registracijaGodina`, `kilometri`, `motor`, `zapremnina`, `snaga`, `mjenjac`, `pogon`, `cijena`, `opis`, `dodan`, `uredjen`) VALUES
(1, 1, 1, '4c', '', 'SUV', '4/5', '6', 'Plava', '5', 2010, '2', 2015, 21543, 'Benzin s plinom', 1000, 213, 'Mehanički mjenjač', 'Prednji', 10000, 'Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. Ovo je opis. ', '2015-04-21 10:23:28', '2015-04-27 19:45:19'),
(2, 1, 3, '4 420', '', 'Monovolumen', '2', '4', 'Plava', '2', 2015, '12', 2015, 100, 'Benzin', 123, 231, 'Automatski mjenjač', '4x4', 12345, 'sdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasda sdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasdasdasdasdasdaasdsad sdasdasdasad sdasdasdasda', '2015-04-21 11:31:17', '2015-04-27 19:45:26'),
(3, 1, 5, 'asd sda wq 123 qw', '', '4x4', '4', '3', 'qwdqw', '2', 2015, '1', 2015, 121212, 'Benzin', 122, 21312, 'Mehanički mjenjač', '4x4', 12312332, 'asd', '2015-04-27 18:12:06', '2015-04-27 20:13:40');

-- --------------------------------------------------------

--
-- Table structure for table `dodatna_oprema`
--

CREATE TABLE IF NOT EXISTS `dodatna_oprema` (
  `id_auta` int(11) NOT NULL,
  `abs` tinyint(4) DEFAULT NULL,
  `esc` tinyint(4) NOT NULL,
  `tcs` tinyint(4) NOT NULL,
  `prednji_jastuci` tinyint(4) NOT NULL,
  `prednji_bocni_jastuci` tinyint(4) NOT NULL,
  `straznji_bocni` tinyint(4) NOT NULL,
  `zracne_zavjese` tinyint(4) NOT NULL,
  `jastuci_koljena` tinyint(4) NOT NULL,
  `isofix` tinyint(4) NOT NULL,
  `daljinsko_zakljucavanje` tinyint(4) NOT NULL,
  `manualna_klima` tinyint(4) NOT NULL,
  `automatska_klima` tinyint(4) NOT NULL,
  `automatska_dvozonska_klima` tinyint(4) NOT NULL,
  `automatska_cetverozonska_klima` tinyint(4) NOT NULL,
  `klimatizirani_pretinac` tinyint(4) NOT NULL,
  `upravljac_koza` tinyint(4) NOT NULL,
  `visina_suvozac` tinyint(4) NOT NULL,
  `sjedala_koza_tkanina` tinyint(4) NOT NULL,
  `unutrasnjost_koza` tinyint(4) NOT NULL,
  `potpora_kicmi` tinyint(4) NOT NULL,
  `el_prednja_sjedala` tinyint(4) NOT NULL,
  `grijanje_prednja` tinyint(4) NOT NULL,
  `grijanje_straznja` tinyint(4) NOT NULL,
  `prednji_naslon` tinyint(4) NOT NULL,
  `straznji_naslon` tinyint(4) NOT NULL,
  `otvor_skije` tinyint(4) NOT NULL,
  `el_poklopac_bunker` tinyint(4) NOT NULL,
  `el_prednja_stakla` tinyint(4) NOT NULL,
  `el_straznja_stakla` tinyint(4) NOT NULL,
  `el_vanjski_ret` tinyint(4) NOT NULL,
  `el_vanjski_ret_i_grijanje` tinyint(4) NOT NULL,
  `el_preklapanje_ret` tinyint(4) NOT NULL,
  `auto_zat_unutar_ret` tinyint(4) NOT NULL,
  `putno_racunalo` tinyint(4) NOT NULL,
  `ekran_u_boji` tinyint(4) NOT NULL,
  `dodirni_zaslon` tinyint(4) NOT NULL,
  `navigacijski_sustav` tinyint(4) NOT NULL,
  `radio_citac` varchar(150) NOT NULL,
  `cd_changer_pet` tinyint(4) NOT NULL,
  `cd_changer_sest` tinyint(4) NOT NULL,
  `audio_upravljac` tinyint(4) NOT NULL,
  `visenam_kolo_upravljaca` tinyint(4) NOT NULL,
  `sd_reader` tinyint(4) NOT NULL,
  `bluetooth_handsfree` tinyint(4) NOT NULL,
  `tempomat` tinyint(4) NOT NULL,
  `head_up_display` tinyint(4) NOT NULL,
  `senzor_svjetla_kise` tinyint(4) NOT NULL,
  `senzor_kise` tinyint(4) NOT NULL,
  `prednji_straznji_park_senzor` tinyint(4) NOT NULL,
  `straznji_park_senzor` tinyint(4) NOT NULL,
  `kamera_rikverc` tinyint(4) NOT NULL,
  `hill_holder` tinyint(4) NOT NULL,
  `nadzor_mrtvog_kuta` tinyint(4) NOT NULL,
  `el_parkirna_koc` tinyint(4) NOT NULL,
  `otklj_bez_kljuca` tinyint(4) NOT NULL,
  `zatamljena_stakla` tinyint(4) NOT NULL,
  `bocna_sjenila` tinyint(4) NOT NULL,
  `straznji_zaslon` tinyint(4) NOT NULL,
  `krovni_prozor` tinyint(4) NOT NULL,
  `panoramski_krov` tinyint(4) NOT NULL,
  `svjetla_magle` tinyint(4) NOT NULL,
  `svjetla_skretanja` tinyint(4) NOT NULL,
  `led_svjetla` tinyint(4) NOT NULL,
  `ksenonke` tinyint(4) NOT NULL,
  `biksenonke` tinyint(4) NOT NULL,
  `auto_duga_svjetla` tinyint(4) NOT NULL,
  `led_straznja_svjetla` tinyint(4) NOT NULL,
  `metalik_boja` tinyint(4) NOT NULL,
  `krovni_nosaci` tinyint(4) NOT NULL,
  `nadzor_tlaka` tinyint(4) NOT NULL,
  `kotaci` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dodatna_oprema`
--

INSERT INTO `dodatna_oprema` (`id_auta`, `abs`, `esc`, `tcs`, `prednji_jastuci`, `prednji_bocni_jastuci`, `straznji_bocni`, `zracne_zavjese`, `jastuci_koljena`, `isofix`, `daljinsko_zakljucavanje`, `manualna_klima`, `automatska_klima`, `automatska_dvozonska_klima`, `automatska_cetverozonska_klima`, `klimatizirani_pretinac`, `upravljac_koza`, `visina_suvozac`, `sjedala_koza_tkanina`, `unutrasnjost_koza`, `potpora_kicmi`, `el_prednja_sjedala`, `grijanje_prednja`, `grijanje_straznja`, `prednji_naslon`, `straznji_naslon`, `otvor_skije`, `el_poklopac_bunker`, `el_prednja_stakla`, `el_straznja_stakla`, `el_vanjski_ret`, `el_vanjski_ret_i_grijanje`, `el_preklapanje_ret`, `auto_zat_unutar_ret`, `putno_racunalo`, `ekran_u_boji`, `dodirni_zaslon`, `navigacijski_sustav`, `radio_citac`, `cd_changer_pet`, `cd_changer_sest`, `audio_upravljac`, `visenam_kolo_upravljaca`, `sd_reader`, `bluetooth_handsfree`, `tempomat`, `head_up_display`, `senzor_svjetla_kise`, `senzor_kise`, `prednji_straznji_park_senzor`, `straznji_park_senzor`, `kamera_rikverc`, `hill_holder`, `nadzor_mrtvog_kuta`, `el_parkirna_koc`, `otklj_bez_kljuca`, `zatamljena_stakla`, `bocna_sjenila`, `straznji_zaslon`, `krovni_prozor`, `panoramski_krov`, `svjetla_magle`, `svjetla_skretanja`, `led_svjetla`, `ksenonke`, `biksenonke`, `auto_duga_svjetla`, `led_straznja_svjetla`, `metalik_boja`, `krovni_nosaci`, `nadzor_tlaka`, `kotaci`) VALUES
(2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 'D asd asd asdsa', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0),
(1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 'CD, DVD, AUX', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 15),
(3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 'CD', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `id_info` int(11) NOT NULL,
  `o_nama` varchar(5000) NOT NULL,
  `telefon` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `financiranje` varchar(2000) NOT NULL,
  `disclaimer` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `marka_auta`
--

CREATE TABLE IF NOT EXISTS `marka_auta` (
`id_marka` int(11) NOT NULL,
  `marka` varchar(30) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `marka_auta`
--

INSERT INTO `marka_auta` (`id_marka`, `marka`) VALUES
(1, 'Alfa Romeo'),
(2, 'Audi'),
(3, 'BMW'),
(4, 'Chevrolet'),
(5, 'Chrysler'),
(6, 'Citroën'),
(7, 'Dacia'),
(8, 'Daihatsu'),
(9, 'Daewoo'),
(10, 'Fiat'),
(11, 'Ford'),
(12, 'Honda'),
(13, 'Hyundai'),
(14, 'Iveco'),
(15, 'Jaguar'),
(16, 'Jeep'),
(17, 'Kia'),
(18, 'Lada'),
(19, 'Lancia'),
(20, 'Land Rover'),
(21, 'Lexus'),
(22, 'Mazda'),
(23, 'Mercedes-Benz'),
(24, 'MINI'),
(25, 'Mitsubishi'),
(26, 'Nissan'),
(27, 'Opel'),
(28, 'Peugeot'),
(29, 'Porsche'),
(30, 'Renault'),
(31, 'Rover'),
(32, 'Saab'),
(33, 'Seat'),
(34, 'Smart'),
(35, 'Subaru'),
(36, 'Suzuki'),
(37, 'Škoda'),
(38, 'Toyota'),
(39, 'Volkswagen'),
(40, 'Volvo');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'ad', 'ad');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auto`
--
ALTER TABLE `auto`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dodatna_oprema`
--
ALTER TABLE `dodatna_oprema`
 ADD PRIMARY KEY (`id_auta`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
 ADD PRIMARY KEY (`id_info`);

--
-- Indexes for table `marka_auta`
--
ALTER TABLE `marka_auta`
 ADD PRIMARY KEY (`id_marka`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auto`
--
ALTER TABLE `auto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `marka_auta`
--
ALTER TABLE `marka_auta`
MODIFY `id_marka` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
