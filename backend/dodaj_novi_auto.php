<?php
    session_start();
    if(!isset($_SESSION["ulogiraniUser"])) header("Location: 404.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Admin panel</title>
       
        <!-- CSS Reset -->
		<link rel="stylesheet" type="text/css" href="reset.css" tppabs="http://www.xooom.pl/work/magicadmin/css/reset.css" media="screen" />
       
        <!-- Fluid 960 Grid System - CSS framework -->
		<link rel="stylesheet" type="text/css" href="grid.css" tppabs="http://www.xooom.pl/work/magicadmin/css/grid.css" media="screen" />
		
        <!-- IE Hacks for the Fluid 960 Grid System -->
        <!--[if IE 6]><link rel="stylesheet" type="text/css" href="ie6.css" tppabs="http://www.xooom.pl/work/magicadmin/css/ie6.css" media="screen" /><![endif]-->
		<!--[if IE 7]><link rel="stylesheet" type="text/css" href="ie.css" tppabs="http://www.xooom.pl/work/magicadmin/css/ie.css" media="screen" /><![endif]-->
        
        <!-- Main stylesheet -->
        <link rel="stylesheet" type="text/css" href="styles.css" tppabs="http://www.xooom.pl/work/magicadmin/css/styles.css" media="screen" />
        
        <!-- WYSIWYG editor stylesheet -->
        <link rel="stylesheet" type="text/css" href="jquery.wysiwyg.css" tppabs="http://www.xooom.pl/work/magicadmin/css/jquery.wysiwyg.css" media="screen" />
        
        <!-- Table sorter stylesheet -->
        <link rel="stylesheet" type="text/css" href="tablesorter.css" tppabs="http://www.xooom.pl/work/magicadmin/css/tablesorter.css" media="screen" />
        
        <!-- Thickbox stylesheet -->
        <link rel="stylesheet" type="text/css" href="thickbox.css" tppabs="http://www.xooom.pl/work/magicadmin/css/thickbox.css" media="screen" />
        
        <!-- Themes. Below are several color themes. Uncomment the line of your choice to switch to different color. All styles commented out means blue theme. -->
        <link rel="stylesheet" type="text/css" href="theme-blue.css" tppabs="http://www.xooom.pl/work/magicadmin/css/theme-blue.css" media="screen" />
        <!--<link rel="stylesheet" type="text/css" href="css/theme-red.css" media="screen" />-->
        <!--<link rel="stylesheet" type="text/css" href="css/theme-yellow.css" media="screen" />-->
        <!--<link rel="stylesheet" type="text/css" href="css/theme-green.css" media="screen" />-->
        <!--<link rel="stylesheet" type="text/css" href="css/theme-graphite.css" media="screen" />-->
        
		<!-- JQuery engine script-->
		<script type="text/javascript" src="jquery-1.3.2.min.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery-1.3.2.min.js"></script>
        
		
		
		<script type="text/javascript" src="moj.js"></script>
        
	</head>
	<body>
		<?php
			include("../php/config.php");
			connect();	
		?>
    	<!-- Header -->
        <div id="header">
            <!-- Header. Status part -->
            <div id="header-status">
                <div class="container_12">
                    <div class="grid_8">
					&nbsp;
                    </div>
                    <div class="grid_4">
                        <a href="logout.php" id="logout">                        Logout
                        </a>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div> <!-- End #header-status -->
            
            <!-- Header. Main part -->
            <div id="header-main">
                <div class="container_12">
                    <div class="grid_12">
                        <div id="logo">
                            <ul id="nav">
                                <li><a href="pregledaj_aute.php">Pregledaj aute</a></li>
                                <li id="current"><a href="">Dodaj auto</a></li>
                            </ul>
                        </div><!-- End. #Logo -->
                    </div><!-- End. .grid_12-->
                    <div style="clear: both;"></div>
                </div><!-- End. .container_12 -->
            </div> <!-- End #header-main -->
            <div style="clear: both;"></div>
        </div> <!-- End #header -->
        
		<div class="container_12">
        

            <!-- Form elements -->    
            <div class="grid_12">
            
                <div class="module">
                     <h2><span>Dodaj novi auto</span></h2>
                        
                     <div class="module-body">
                        <form method="POST" action="unesi_auto.php" id="forma" onsubmit="return validacija()">
							
							 <p>
                                <label>Marka:</label>
                                <select id="select_marka" name="select_marka"  class="input-short">
                                    <option value='0' selected="selected">Odaberi marku</option>
									<?php
										$sql= mysql_query ("SELECT * FROM marka_auta");
										while ($row = mysql_fetch_array($sql)){											
											echo "<option value='".$row['id_marka']."'>" .$row['marka']. "</option>";
										}
									?>	
                                </select>
								<span id="marka_error" style="display:none;" class="notification-input ni-error">Odaberite vrijednost!</span>
                            </p>
							
						
							<p>
                                <label>Model </label>
                                <input type="text"  name="model_dodatno" placeholder="Dodatno" class="input-short" />
								<span id="model_dodatno_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							<p>
                                <label>Karoserija</label>
								<select id="select_karoserija"  name="select_karoserija" class="input-short">
									<option value='0' selected="selected">Odaberi karoseriju</option>		
									<option value='4x4' >4x4</option>
									<option value='Coupe' >Coupe</option>
									<option value='Gradski' >Gradski</option>
									<option value='Karavan' >Karavan</option>
									<option value='Kabriolet' >Kabriolet</option>
									<option value='Kombi i dostavna vozila' >Kombi i dostavna vozila</option>
									<option value='Kombi i dostavna vozila' >Kombi i dostavna vozila</option>
									<option value='Limuzina' >Limuzina</option>
									<option value='Monovolumen' >Monovolumen</option>
									<option value='Putnička kombi vozila' >Putnička kombi vozila</option>
									<option value='SUV' >SUV</option>
									<option value='Teretna vozila' >Teretna vozila</option>
                                </select>
                                <span id="karoserija_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
                            <p>
                                <label>Vrata</label>
                                <select id="select_vrata" name="select_vrata" class="input-short">
                                    <option value='0' selected="selected">Odaberite vrijednost</option>
                                    <option value='2'>2</option>
									<option value='2/3'>2/3</option>
									<option value='4'>4</option>
									<option value='4/5'>4/5</option>
									<option value='4/5'>6</option>
                                </select>
								<span id="vrata_error" style="display:none;" class="notification-input ni-error">Odaberite vrijednost!</span>
                            </p>
                            <p>
                                <label>Sjedala</label>
                                <select id="select_sjedala" name="select_sjedala" class="input-short">
									<option value='0' selected="selected">Odaberite vrijednost</option>
									<option value='2'>2</option>
									<option value='3'>3</option>
									<option value='4'>4</option>
									<option value='5'>5</option>
									<option value='6'>6</option>
									<option value='7'>7</option>
									<option value='8'>8</option>
									<option value='9'>9</option>
									<option value='9+'>9+</option>
                                </select>
								<span id="sjedala_error" style="display:none;" class="notification-input ni-error">Odaberite vrijednost!</span>
                            </p>
							
							<p>
                                <label>Boja</label>
                                <input type="text" name="boja" placeholder="Boja" class="input-short" />
								<span id="boja_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							<p>
                                <label>Godina proizvodnje</label>
								<?php
									
									echo '<select id="select_mjesec" name="select_mjesec" class="input-short">
									<option value="0" selected="selected">Odaberite mjesec</option>';
									for($i = 1; $i <= 12 ; $i++) echo '<option value="'.$i.'">'.$i.'</option>';
									echo "</select>";
								
								?>
								<?php
									
									echo '<select id="select_godina" name="select_godina" class="input-short">
												<option value="0" selected="selected">Odaberite godinu</option>';
									for($i = date("Y"); $i >= 1970 ; $i--) echo '<option value="'.$i.'">'.$i.'</option>';
									echo "</select>";
								
								?>
								<span id="godina_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
								<span id="mjesec_error" style="display:none;" class="notification-input ni-error">Odaberite vrijednost!</span>
                            </p>
                            
							<p>
                                <label>Registracija do</label>
								<?php
									
									echo '<select id="select_reg_mjesec" name="select_reg_mjesec" class="input-short">
									<option value="0" selected="selected">Odaberite mjesec ili ovo, ako auto nije registriran</option>';
									for($i = 1; $i <= 12 ; $i++) echo '<option value="'.$i.'">'.$i.'</option>';
									echo "</select>";
								
								?>
								<?php
									
									echo '<select id="select_reg_godina" name="select_reg_godina" class="input-short">
												<option value="0" selected="selected">Odaberite godinu ili ovo, ako auto nije registriran</option>';
									for($i = date("Y"); $i <= 2030 ; $i++) echo '<option value="'.$i.'">'.$i.'</option>';
									echo "</select>";
								
								?>
                            </p>
                            
							<p>
                                <label>Kilometri</label>
                                <input type="text" name="kilometri" placeholder="Kilometri" class="input-short" />
								<span id="kilometri_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							<p>
                                <label>Motor</label>
                                <select id="select_motor" name="select_motor" class="input-short">
                                    <option value='0' selected="selected">Odaberite vrijednost</option>
                                    <option value='Benzin'>Benzin</option>
									<option value='Benzin s plinom'>Benzin s plinom</option>
									<option value='Diesel'>Diesel</option>
                                </select>
								<span id="motor_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							<p>
                                <label>Zapremnina [cm3]</label>
                                <input type="text" name="zapremnina" placeholder="Zapremnina" class="input-short" />
								<span id="zapremnina_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							<p>
                                <label>Snaga [kW]</label>
                                <input type="text" name="snaga" placeholder="Snaga" class="input-short" />
								<span id="snaga_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							<p>
                                <label>Mjenjac</label>
                                <select id="select_mjenjac" name="select_mjenjac" class="input-short">
                                    <option value='0' selected="selected">Odaberite vrijednost</option>
                                    <option value='Mehanički mjenjač'>Mehanički mjenjač</option>
									<option value='Automatski mjenjač'>Automatski mjenjač</option>
                                </select>
								<span id="mjenjac_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							<p>
                                <label>Pogon</label>
                                <select id="select_pogon" name="select_pogon" class="input-short">
                                    <option value='0' selected="selected">Odaberite vrijednost</option>
                                    <option value='4x4'>4x4</option>
                                    <option value='Prednji'>Prednji</option>
                                    <option value='Zadnji'>Zadnji</option>
                                </select>
								<span id="pogon_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							<p>
                                <label>Cijena [EUR]</label>
                                <input type="text" name="cijena" placeholder="Cijena" class="input-short" />
								<span id="cijena_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							
							<p>
                                <label>Opis</label>
                                <textarea id="opis_auta" rows="7" cols="90" name="opis_auta" form="forma" class="input-short"></textarea>
								<span id="opis_error" style="display:none;" class="notification-input ni-error">Unesite vrijednost!</span>
                            </p>
							
							
                            <fieldset>
                                <input class="submit-green" type="submit" value="Unesi auto" /> 
                            </fieldset>
                        </form>
                     </div> <!-- End .module-body -->

                </div>  <!-- End .module -->
        		<div style="clear:both;"></div>
            </div> <!-- End .grid_12 -->
                
            
            <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
		
		<div id="kontejner"></div>
	</body>
</html>