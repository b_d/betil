<?php
    session_start();
    if(!isset($_SESSION["ulogiraniUser"])) header("Location: 404.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Admin panel</title>

        <!-- CSS Reset -->
		<link rel="stylesheet" type="text/css" href="reset.css" tppabs="http://www.xooom.pl/work/magicadmin/css/reset.css" media="screen" />

        <!-- Fluid 960 Grid System - CSS framework -->
		<link rel="stylesheet" type="text/css" href="grid.css" tppabs="http://www.xooom.pl/work/magicadmin/css/grid.css" media="screen" />

        <!-- IE Hacks for the Fluid 960 Grid System -->
        <!--[if IE 6]><link rel="stylesheet" type="text/css" href="ie6.css" tppabs="http://www.xooom.pl/work/magicadmin/css/ie6.css" media="screen" /><![endif]-->
		<!--[if IE 7]><link rel="stylesheet" type="text/css" href="ie.css" tppabs="http://www.xooom.pl/work/magicadmin/css/ie.css" media="screen" /><![endif]-->

        <!-- Main stylesheet -->
        <link rel="stylesheet" type="text/css" href="styles.css" tppabs="http://www.xooom.pl/work/magicadmin/css/styles.css" media="screen" />

        <!-- WYSIWYG editor stylesheet -->
        <link rel="stylesheet" type="text/css" href="jquery.wysiwyg.css" tppabs="http://www.xooom.pl/work/magicadmin/css/jquery.wysiwyg.css" media="screen" />

        <!-- Table sorter stylesheet -->
        <link rel="stylesheet" type="text/css" href="tablesorter.css" tppabs="http://www.xooom.pl/work/magicadmin/css/tablesorter.css" media="screen" />

        <!-- Thickbox stylesheet -->
        <link rel="stylesheet" type="text/css" href="thickbox.css" tppabs="http://www.xooom.pl/work/magicadmin/css/thickbox.css" media="screen" />

        <!-- Themes. Below are several color themes. Uncomment the line of your choice to switch to different color. All styles commented out means blue theme. -->
        <link rel="stylesheet" type="text/css" href="theme-blue.css"  media="screen" />
        <!--<link rel="stylesheet" type="text/css" href="css/theme-red.css" media="screen" />-->
        <!--<link rel="stylesheet" type="text/css" href="css/theme-yellow.css" media="screen" />-->
        <!--<link rel="stylesheet" type="text/css" href="css/theme-green.css" media="screen" />-->
        <!--<link rel="stylesheet" type="text/css" href="css/theme-graphite.css" media="screen" />-->

		<!-- JQuery engine script-->
		<script type="text/javascript" src="jquery-1.3.2.min.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery-1.3.2.min.js"></script>

		<!-- JQuery WYSIWYG plugin script -->
		<script type="text/javascript" src="jquery.wysiwyg.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery.wysiwyg.js"></script>

        <!-- JQuery tablesorter plugin script-->
		<script type="text/javascript" src="jquery.tablesorter.min.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery.tablesorter.min.js"></script>

		<!-- JQuery pager plugin script for tablesorter tables -->
		<script type="text/javascript" src="jquery.tablesorter.pager.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery.tablesorter.pager.js"></script>

		<!-- JQuery password strength plugin script -->
		<script type="text/javascript" src="jquery.pstrength-min.1.2.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery.pstrength-min.1.2.js"></script>
		<script type="text/javascript" src="moj.js" ></script>
        
    <script src='pdfmake.min.js'></script>
 	<script src='vfs_fonts.js'></script>
        
        <!-- Initiate WYIWYG text area -->
		<script type="text/javascript">
			$(function()
			{
			$('#wysiwyg').wysiwyg(
			{
			controls : {
			separator01 : { visible : true },
			separator03 : { visible : true },
			separator04 : { visible : true },
			separator00 : { visible : true },
			separator07 : { visible : false },
			separator02 : { visible : false },
			separator08 : { visible : false },
			insertOrderedList : { visible : true },
			insertUnorderedList : { visible : true },
			undo: { visible : true },
			redo: { visible : true },
			justifyLeft: { visible : true },
			justifyCenter: { visible : true },
			justifyRight: { visible : true },
			justifyFull: { visible : true },
			subscript: { visible : true },
			superscript: { visible : true },
			underline: { visible : true },
            increaseFontSize : { visible : false },
            decreaseFontSize : { visible : false }
			}
			} );
			});
        </script>

        <!-- Initiate tablesorter script -->
        <script type="text/javascript">
			$(document).ready(function() {
				$("#myTable")
				.tablesorter({
					// zebra coloring
					widgets: ['zebra'],
					// pass the headers argument and assing a object
					headers: {
						// assign the sixth column (we start counting zero)
						9: {
							// disable it by setting the property sorter to false
							sorter: false
						}
					}
				})
			.tablesorterPager({container: $("#pager")});
		});
		</script>

        <!-- Initiate password strength script -->
		<script type="text/javascript">
			$(function() {
			$('.password').pstrength();
			});
        </script>
	</head>
	<body>
	<?php
			include("../php/config.php");
			connect();
	?>
    	<!-- Header -->
        <div id="header">
            <!-- Header. Status part -->
            <div id="header-status">
                <div class="container_12">
                    <div class="grid_8">
					&nbsp;
                    </div>
                    <div class="grid_4">
                        <a href="logout.php" id="logout">                        Logout
                        </a>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div> <!-- End #header-status -->

            <!-- Header. Main part -->
            <div id="header-main">
                <div class="container_12">
                    <div class="grid_12">
                        <div id="logo">
                            <ul id="nav">
                                <li id="current"><a href="pregledaj_aute.php">Pregledaj aute</a></li>
                                <li><a href="dodaj_novi_auto.php">Dodaj auto</a></li>
                            </ul>
                        </div><!-- End. #Logo -->
                    </div><!-- End. .grid_12-->
                    <div style="clear: both;"></div>
                </div><!-- End. .container_12 -->
            </div> <!-- End #header-main -->
            <div style="clear: both;"></div>

        </div> <!-- End #header -->

		<div class="container_12">


                <!-- Example table -->
                <div class="module">
                	<h2><span>Svi auti</span></h2>

                    <div class="module-table-body">
                    	<form action="">
                        <table id="myTable" class="tablesorter">
                        	<thead>
                                <tr >
                                    <th style="width:10%; text-align:center;" >Marka</th>
                                    <th style="width:10%; text-align:center;">Model</th>
                                    <th style="width:10%; text-align:center;">Godište</th>
                                    <th style="width:10%; text-align:center;">Kilometraža</th>
                                    <th style="width:10%; text-align:center;">Motor</th>
                                    <th style="width:5%; text-align:center;">Boja</th>
                                    <th style="width:10%; text-align:center;">Cijena [EUR]</th>
                                    <th style="width:10%; text-align:center;">Uređen</th>
                                    <th style="width:6%; text-align:center;">Aktivan</th>
                                    <th style="width:12%; text-align:center;">Akcije</th>
                                </tr>
                            </thead>
                            <tbody>

								<?php

									$sql= mysql_query ("SELECT auto.id,auto.aktivan, auto.uredjen, marka_auta.marka, auto.model_dodatno, auto.cijena, auto.dodan , auto.godina, auto.kilometri, auto.motor, auto.boja
														FROM `auto`,`marka_auta`
														WHERE auto.marka = marka_auta.id_marka");
									while ($row = mysql_fetch_array($sql)){
										echo "
										<tr>
											<td class='align-center'>".$row['marka']."</td>
											<td class='align-center'>".$row['model_dodatno']."</td>
											<td class='align-center'>".$row['godina']."</td>
											<td class='align-center'>".$row['kilometri']."</td>
											<td class='align-center'>".$row['motor']."</td>
											<td class='align-center'>".$row['boja']."</td>
											<td class='align-center'>".$row['cijena']."</td>
											<td class='align-center'>".$row['uredjen']."</td>
											<td class='align-center'>"; if($row['aktivan'] == '0') echo "Ne"; else echo "Da";echo "</td>
											<td class='align-center'>
												<a href='uredi_auto.php?id=".$row['id']."'><img src='pencil.gif'  width='16' height='16' title='Uredi' /></a>&nbsp;&nbsp;
												<a href='dodaj_dodatno.php?id=".$row['id']."'><img src='dodaj.jpg'  width='16' height='16' title='Dodatno' /></a>&nbsp;&nbsp;
												<img src='minus-circle.gif'  onclick='validacija_brisanja(".$row['id'].")' width='16' height='16' title='Briši' />&nbsp;&nbsp;
												<a href='slike.php?id=".$row['id']."'><img src='slike.png'  width='16' height='16'  title='Galerija' /></a>&nbsp;&nbsp;
												<a href='#' onclick='pokreni_spremanje(".$row['id'].");'><img src='pdf.gif'  width='16' height='16'  title='Skini pdf' /></a>
											</td>
										</tr>  ";
									}
								?>
                            </tbody>
                        </table>
                        </form>
                        <div class="pager" id="pager">
                            <form action="">
                                <div>
                                <img class="first" src="arrow-stop-180.gif"  alt="first"/>
                                <img class="prev" src="arrow-180.gif" alt="prev"/>
                                <input type="text" class="pagedisplay input-short align-center"/>
                                <img class="next" src="arrow.gif"  alt="next"/>
                                <img class="last" src="arrow-stop.gif"  alt="last"/>
                                <select class="pagesize input-short align-center">
                                    <option value="30" selected="selected">30</option>
                                    <option value="60">60</option>
                                    <option value="90">90</option>
                                    <option value="120">120</option>
                                </select>
                                </div>
                            </form>
                        </div>

                        <div style="clear: both"></div>
                     </div> <!-- End .module-table-body -->
                </div> <!-- End .module -->




			</div> <!-- End .grid_12 -->


            <div style="clear:both;"></div>


            <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->
				<div id="kontejner"></div>

	</body>
</html>
