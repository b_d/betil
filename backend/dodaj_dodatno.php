<?php
    session_start();
    if(!isset($_SESSION["ulogiraniUser"])) header("Location: 404.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Admin panel</title>

        <!-- CSS Reset -->
		<link rel="stylesheet" type="text/css" href="reset.css" tppabs="http://www.xooom.pl/work/magicadmin/css/reset.css" media="screen" />

        <!-- Fluid 960 Grid System - CSS framework -->
		<link rel="stylesheet" type="text/css" href="grid.css" tppabs="http://www.xooom.pl/work/magicadmin/css/grid.css" media="screen" />

        <!-- IE Hacks for the Fluid 960 Grid System -->
        <!--[if IE 6]><link rel="stylesheet" type="text/css" href="ie6.css" tppabs="http://www.xooom.pl/work/magicadmin/css/ie6.css" media="screen" /><![endif]-->
		<!--[if IE 7]><link rel="stylesheet" type="text/css" href="ie.css" tppabs="http://www.xooom.pl/work/magicadmin/css/ie.css" media="screen" /><![endif]-->

        <!-- Main stylesheet -->
        <link rel="stylesheet" type="text/css" href="styles.css" tppabs="http://www.xooom.pl/work/magicadmin/css/styles.css" media="screen" />

        <!-- WYSIWYG editor stylesheet -->
        <link rel="stylesheet" type="text/css" href="jquery.wysiwyg.css" tppabs="http://www.xooom.pl/work/magicadmin/css/jquery.wysiwyg.css" media="screen" />

        <!-- Table sorter stylesheet -->
        <link rel="stylesheet" type="text/css" href="tablesorter.css" tppabs="http://www.xooom.pl/work/magicadmin/css/tablesorter.css" media="screen" />

        <!-- Thickbox stylesheet -->
        <link rel="stylesheet" type="text/css" href="thickbox.css" tppabs="http://www.xooom.pl/work/magicadmin/css/thickbox.css" media="screen" />

        <!-- Themes. Below are several color themes. Uncomment the line of your choice to switch to different color. All styles commented out means blue theme. -->
        <link rel="stylesheet" type="text/css" href="theme-blue.css"  media="screen" />
        <!--<link rel="stylesheet" type="text/css" href="css/theme-red.css" media="screen" />-->
        <!--<link rel="stylesheet" type="text/css" href="css/theme-yellow.css" media="screen" />-->
        <!--<link rel="stylesheet" type="text/css" href="css/theme-green.css" media="screen" />-->
        <!--<link rel="stylesheet" type="text/css" href="css/theme-graphite.css" media="screen" />-->

		<!-- JQuery engine script-->
		<script type="text/javascript" src="jquery-1.3.2.min.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery-1.3.2.min.js"></script>

		<!-- JQuery WYSIWYG plugin script -->
		<script type="text/javascript" src="jquery.wysiwyg.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery.wysiwyg.js"></script>

        <!-- JQuery tablesorter plugin script-->
		<script type="text/javascript" src="jquery.tablesorter.min.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery.tablesorter.min.js"></script>

		<!-- JQuery pager plugin script for tablesorter tables -->
		<script type="text/javascript" src="jquery.tablesorter.pager.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery.tablesorter.pager.js"></script>

		<!-- JQuery password strength plugin script -->
		<script type="text/javascript" src="jquery.pstrength-min.1.2.js" tppabs="http://www.xooom.pl/work/magicadmin/js/jquery.pstrength-min.1.2.js"></script>

		<!-- JQuery thickbox plugin script -->

        <script type="text/javascript" src="moj.js"></script>
        <!-- Initiate WYIWYG text area -->
		<script type="text/javascript">
			$(function()
			{
			$('#wysiwyg').wysiwyg(
			{
			controls : {
			separator01 : { visible : true },
			separator03 : { visible : true },
			separator04 : { visible : true },
			separator00 : { visible : true },
			separator07 : { visible : false },
			separator02 : { visible : false },
			separator08 : { visible : false },
			insertOrderedList : { visible : true },
			insertUnorderedList : { visible : true },
			undo: { visible : true },
			redo: { visible : true },
			justifyLeft: { visible : true },
			justifyCenter: { visible : true },
			justifyRight: { visible : true },
			justifyFull: { visible : true },
			subscript: { visible : true },
			superscript: { visible : true },
			underline: { visible : true },
            increaseFontSize : { visible : false },
            decreaseFontSize : { visible : false }
			}
			} );
			});
        </script>

        <!-- Initiate tablesorter script -->
        <script type="text/javascript">
			$(document).ready(function() {
				$("#myTable")
				.tablesorter({
					// zebra coloring
					widgets: ['zebra'],
					// pass the headers argument and assing a object
					headers: {
						// assign the sixth column (we start counting zero)
						8: {
							// disable it by setting the property sorter to false
							sorter: false
						}
					}
				})
			.tablesorterPager({container: $("#pager")});
		});
		</script>

        <!-- Initiate password strength script -->
		<script type="text/javascript">
			$(function() {
			$('.password').pstrength();
			});
        </script>
	</head>
	<body>
	<?php
			include("../php/config.php");
			connect();
	?>
    	<!-- Header -->
        <div id="header">
            <!-- Header. Status part -->
            <div id="header-status">
                <div class="container_12">
                    <div class="grid_8">
					&nbsp;
                    </div>
                    <div class="grid_4">
                        <a href="logout.php" id="logout">                        Logout
                        </a>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div> <!-- End #header-status -->

            <!-- Header. Main part -->
            <div id="header-main">
                <div class="container_12">
                    <div class="grid_12">
                        <div id="logo">
                            <ul id="nav">
                                <li id="current"><a href="pregledaj_aute.php">Pregledaj aute</a></li>
                                <li><a href="dodaj_novi_auto.php">Dodaj auto</a></li>
                            </ul>
                        </div><!-- End. #Logo -->
                    </div><!-- End. .grid_12-->
                    <div style="clear: both;"></div>
                </div><!-- End. .container_12 -->
            </div> <!-- End #header-main -->
            <div style="clear: both;"></div>

        </div> <!-- End #header -->

		<div class="container_12">


                <!-- Example table -->
                <div class="module">
                	<h2><span>Dodatna oprema</span></h2>
					 <div class="module-body">
                     <fieldset>
							<form method="POST" action="unesi_opremu.php">
								<div style="float: left;">
                                <legend><b>Označite opremu koju auto posjeduje:</b></legend>
                                <ul >
								<?php
									$id = $_GET['id'];

									$sql= mysql_query ("SELECT * from dodatna_oprema where id_auta = $id");
									$row = mysql_fetch_array($sql);

									echo "<li><label><input type='checkbox' name='abs'"; if($row['abs'] == 1) echo "checked='checked'";  echo 'id="cb1" />ABS - sustav protiv blokiranja kočionog sistema</label></li>';
									echo "<li><label><input type='checkbox' name='esc'"; if($row['esc'] == 1) echo "checked='checked'";  echo 'id="cb1" />ESC (ESP/DSC) - elektronski sustav stabilnosti</label></li>';
									echo "<li><label><input type='checkbox' name='tcs'"; if($row['tcs'] == 1) echo "checked='checked'";  echo 'id="cb1" />TCS/ASR - regulacija pogonskog proklizavanja</label></li>';
									echo "<li><label><input type='checkbox' name='prednji_jastuci'"; if($row['prednji_jastuci'] == 1) echo "checked='checked'";  echo 'id="cb1" />Prednji zračni jastuci</label></li>';
									echo "<li><label><input type='checkbox' name='prednji_bocni_jastuci'"; if($row['prednji_bocni_jastuci'] == 1) echo "checked='checked'";  echo 'id="cb1" />Prednji bočni zračni jastuci</label></li>';
									echo "<li><label><input type='checkbox' name='straznji_bocni'"; if($row['straznji_bocni'] == 1) echo "checked='checked'";  echo 'id="cb1" />Stražnji bočni zračni jastuci</label></li>';
									echo "<li><label><input type='checkbox' name='zracne_zavjese'"; if($row['zracne_zavjese'] == 1) echo "checked='checked'";  echo 'id="cb1" />Zračne zavjese </label></li>';
									echo "<li><label><input type='checkbox' name='jastuci_koljena'"; if($row['jastuci_koljena'] == 1) echo "checked='checked'";  echo 'id="cb1" />Zračni jastuci za koljena</label></li>';
									echo "<li><label><input type='checkbox' name='isofix'"; if($row['isofix'] == 1) echo "checked='checked'";  echo 'id="cb1" />Isofix (sustav učvršćivanja dječjih sjedalica)</label></li>';
									echo "<li><label><input type='checkbox' name='daljinsko_zakljucavanje'"; if($row['daljinsko_zakljucavanje'] == 1) echo "checked='checked'";  echo 'id="cb1" />Daljinsko zaključavanje</label></li>';
									echo "<li><label><input type='checkbox' name='manualna_klima'"; if($row['manualna_klima'] == 1) echo "checked='checked'";  echo 'id="cb1" />Manualni klima uređaj</label></li>';
									echo "<li><label><input type='checkbox' name='automatska_klima'"; if($row['automatska_klima'] == 1) echo "checked='checked'";  echo 'id="cb1" />Automatski klima uređaj</label></li>';
									echo "<li><label><input type='checkbox' name='automatska_dvozonska_klima'"; if($row['automatska_dvozonska_klima'] == 1) echo "checked='checked'";  echo 'id="cb1" />Automatski dvozonski klima uređaj</label></li>';
									echo "<li><label><input type='checkbox' name='automatska_cetverozonska_klima'"; if($row['automatska_cetverozonska_klima'] == 1) echo "checked='checked'";  echo 'id="cb1" />Automatski četverozonski klima uređaj</label></li>';
									echo "<li><label><input type='checkbox' name='klimatizirani_pretinac'"; if($row['klimatizirani_pretinac'] == 1) echo "checked='checked'";  echo 'id="cb1" />Klimatizirani pretinac</label></li>';
									echo "<li><label><input type='checkbox' name='upravljac_koza'"; if($row['upravljac_koza'] == 1) echo "checked='checked'";  echo 'id="cb1" />Upravljač presvučen kožom</label></li>';
									echo "<li><label><input type='checkbox' name='visina_suvozac'"; if($row['visina_suvozac'] == 1) echo "checked='checked'";  echo 'id="cb1" />Namještanje visine suvozačevog sjedala</label></li>';
									echo "<li><label><input type='checkbox' name='sjedala_koza_tkanina'"; if($row['sjedala_koza_tkanina'] == 1) echo "checked='checked'";  echo 'id="cb1" />Sjedala u kombinaciji koža tkanina</label></li>';
									echo "<li><label><input type='checkbox' name='unutrasnjost_koza'"; if($row['unutrasnjost_koza'] == 1) echo "checked='checked'";  echo 'id="cb1" />Unutrašnjost u koži</label></li>';
									echo "<li><label><input type='checkbox' name='potpora_kicmi'"; if($row['potpora_kicmi'] == 1) echo "checked='checked'";  echo 'id="cb1" />Potpora za kralježnicu</label></li>';
									echo "<li><label><input type='checkbox' name='el_prednja_sjedala'"; if($row['el_prednja_sjedala'] == 1) echo "checked='checked'";  echo 'id="cb1" />Električna prilagodba prednjih sjedala</label></li>';
									echo "<li><label><input type='checkbox' name='grijanje_prednja'"; if($row['grijanje_prednja'] == 1) echo "checked='checked'";  echo 'id="cb1" />Grijanje prednjih sjedala</label></li>';
									echo "<li><label><input type='checkbox' name='grijanje_straznja'"; if($row['grijanje_straznja'] == 1) echo "checked='checked'";  echo 'id="cb1" />Grijanje stražnjih sjedala</label></li>';
									echo "<li><label><input type='checkbox' name='prednji_naslon'"; if($row['prednji_naslon'] == 1) echo "checked='checked'";  echo 'id="cb1" />Prednji naslon za ruke</label></li>';
									echo "<li><label><input type='checkbox' name='straznji_naslon'"; if($row['straznji_naslon'] == 1) echo "checked='checked'";  echo 'id="cb1" />Stražnji naslon za ruke</label></li>';
									echo "<li><label><input type='checkbox' name='otvor_skije'"; if($row['otvor_skije'] == 1) echo "checked='checked'";  echo 'id="cb1" />Otvor za skije</label></li>';
									echo "<li><label><input type='checkbox' name='el_poklopac_bunker'"; if($row['el_poklopac_bunker'] == 1) echo "checked='checked'";  echo 'id="cb1" />Električni poklopac prtljažnika</label></li>';
									echo "<li><label><input type='checkbox' name='el_prednja_stakla'"; if($row['el_prednja_stakla'] == 1) echo "checked='checked'";  echo 'id="cb1" />Električno podizanje prednjih stakala</label></li>';
									echo "<li><label><input type='checkbox' name='el_straznja_stakla'"; if($row['el_straznja_stakla'] == 1) echo "checked='checked'";  echo 'id="cb1" />Električno podizanje stražnjih stakala</label></li>';
									echo "<li><label><input type='checkbox' name='el_vanjski_ret'"; if($row['el_vanjski_ret'] == 1) echo "checked='checked'";  echo 'id="cb1" />Električno namještanje zrcala vanjskih retrovizora</label></li>';
									echo "<li><label><input type='checkbox' name='el_vanjski_ret_i_grijanje'"; if($row['el_vanjski_ret_i_grijanje'] == 1) echo "checked='checked'";  echo 'id="cb1" />Elelktično grijanje i namještanje zrcala vanjskih retrovizora </label></li>';
									echo "<li><label><input type='checkbox' name='el_preklapanje_ret'"; if($row['el_preklapanje_ret'] == 1) echo "checked='checked'";  echo 'id="cb1" />Električno preklapanje vanjskih retrovizora</label></li>';
									echo "<li><label><input type='checkbox' name='auto_zat_unutar_ret'"; if($row['auto_zat_unutar_ret'] == 1) echo "checked='checked'";  echo 'id="cb1" />Automatsko zatamnjivanje unutarnjeg retrovizora</label></li>';
									echo "<li><label><input type='checkbox' name='putno_racunalo'"; if($row['putno_racunalo'] == 1) echo "checked='checked'";  echo 'id="cb1" />Putno računalo</label></li>';
									echo "<li><label><input type='checkbox' name='ekran_u_boji'"; if($row['ekran_u_boji'] == 1) echo "checked='checked'";  echo 'id="cb1" />Višenamjenski ekran u boji</label></li>';
									echo "<li><label><input type='checkbox' name='dodirni_zaslon'"; if($row['dodirni_zaslon'] == 1) echo "checked='checked'";  echo 'id="cb1" />Dodirni zaslon </label></li>';
									echo "<li><label><input type='checkbox' name='navigacijski_sustav'"; if($row['navigacijski_sustav'] == 1) echo "checked='checked'";  echo 'id="cb1" />Navigacijski sustav</label></li>';



echo "<li><label>Radio sa <input type='text' name='radio_citac' size='50' maxlength='50' value='".$row['radio_citac']."'  /> čitačem</label></li>";

									echo "<li><label><input type='checkbox' name='cd_changer_pet'"; if($row['cd_changer_pet'] == 1) echo "checked='checked'";  echo 'id="cb1" />5 CD changer</label></li>';
									echo "<li><label><input type='checkbox' name='cd_changer_sest'"; if($row['cd_changer_sest'] == 1) echo "checked='checked'";  echo 'id="cb1" />6 CD changer</label></li>';
									echo "<li><label><input type='checkbox' name='audio_upravljac'"; if($row['audio_upravljac'] == 1) echo "checked='checked'";  echo 'id="cb1" />Upravljanje audio opremom s upravljača</label></li>';
									echo "<li><label><input type='checkbox' name='visenam_kolo_upravljaca'"; if($row['visenam_kolo_upravljaca'] == 1) echo "checked='checked'";  echo 'id="cb1" />Višenamjensko kolo upravljača</label></li>';
									echo "<li><label><input type='checkbox' name='sd_reader'"; if($row['sd_reader'] == 1) echo "checked='checked'";  echo 'id="cb1" />Čitač SD kartice</label></li>';
									echo "<li><label><input type='checkbox' name='bluetooth_handsfree'"; if($row['bluetooth_handsfree'] == 1) echo "checked='checked'";  echo 'id="cb1" />Bluetooth Handsfree</label></li>';
									echo "<li><label><input type='checkbox' name='tempomat'"; if($row['tempomat'] == 1) echo "checked='checked'";  echo 'id="cb1" />Tempomat - regulator brzine</label></li>';
									echo "<li><label><input type='checkbox' name='head_up_display'"; if($row['head_up_display'] == 1) echo "checked='checked'";  echo 'id="cb1" />Head up display</label></li>';
									echo "<li><label><input type='checkbox' name='senzor_svjetla_kise'"; if($row['senzor_svjetla_kise'] == 1) echo "checked='checked'";  echo 'id="cb1" />Senzor za svjetla i kišu</label></li>';
									echo "<li><label><input type='checkbox' name='senzor_kise'"; if($row['senzor_kise'] == 1) echo "checked='checked'";  echo 'id="cb1" />Senzor za kišu</label></li>';
									echo "<li><label><input type='checkbox' name='prednji_straznji_park_senzor'"; if($row['prednji_straznji_park_senzor'] == 1) echo "checked='checked'";  echo 'id="cb1" />Prednji i stražnji parking senzori </label></li>';
									echo "<li><label><input type='checkbox' name='straznji_park_senzor'"; if($row['straznji_park_senzor'] == 1) echo "checked='checked'";  echo 'id="cb1" />Stražnji parking senzori</label></li>';
									echo "<li><label><input type='checkbox' name='kamera_rikverc'"; if($row['kamera_rikverc'] == 1) echo "checked='checked'";  echo 'id="cb1" />Kamera za vožnju unazad</label></li>';
									echo "<li><label><input type='checkbox' name='hill_holder'"; if($row['hill_holder'] == 1) echo "checked='checked'";  echo 'id="cb1" />Hill holder</label></li>';
									echo "<li><label><input type='checkbox' name='nadzor_mrtvog_kuta'"; if($row['nadzor_mrtvog_kuta'] == 1) echo "checked='checked'";  echo 'id="cb1" />Sustav nadzora mrtvog kuta </label></li>';
									echo "<li><label><input type='checkbox' name='el_parkirna_koc'"; if($row['el_parkirna_koc'] == 1) echo "checked='checked'";  echo 'id="cb1" />Električna parkirna kočnica</label></li>';
									echo "<li><label><input type='checkbox' name='otklj_bez_kljuca'"; if($row['otklj_bez_kljuca'] == 1) echo "checked='checked'";  echo 'id="cb1" />Otključavanje i pokretanje vozila bez ključa</label></li>';
									echo "<li><label><input type='checkbox' name='zatamljena_stakla'"; if($row['zatamljena_stakla'] == 1) echo "checked='checked'";  echo 'id="cb1" />Zatamnjena stakla od B stupa</label></li>';
									echo "<li><label><input type='checkbox' name='bocna_sjenila'"; if($row['bocna_sjenila'] == 1) echo "checked='checked'";  echo 'id="cb1" />Bočna sjenila u drugom redu</label></li>';
									echo "<li><label><input type='checkbox' name='straznji_zaslon'"; if($row['straznji_zaslon'] == 1) echo "checked='checked'";  echo 'id="cb1" />Stražnji električni zastor</label></li>';
									echo "<li><label><input type='checkbox' name='krovni_prozor'"; if($row['krovni_prozor'] == 1) echo "checked='checked'";  echo 'id="cb1" />Krovni prozor</label></li>';
									echo "<li><label><input type='checkbox' name='panoramski_krov'"; if($row['panoramski_krov'] == 1) echo "checked='checked'";  echo 'id="cb1" />Stakleni panoramski krov</label></li>';
									echo "<li><label><input type='checkbox' name='svjetla_magle'"; if($row['svjetla_magle'] == 1) echo "checked='checked'";  echo 'id="cb1" />Svjetla za maglu</label></li>';
									echo "<li><label><input type='checkbox' name='svjetla_skretanja'"; if($row['svjetla_skretanja'] == 1) echo "checked='checked'";  echo 'id="cb1" />Svjetla za skretanje</label></li>';
									echo "<li><label><input type='checkbox' name='led_svjetla'"; if($row['led_svjetla'] == 1) echo "checked='checked'";  echo 'id="cb1" />LED svjetla </label></li>';
									echo "<li><label><input type='checkbox' name='ksenonke'"; if($row['ksenonke'] == 1) echo "checked='checked'";  echo 'id="cb1" />Xenon glavna svjetla</label></li>';
									echo "<li><label><input type='checkbox' name='biksenonke'"; if($row['biksenonke'] == 1) echo "checked='checked'";  echo 'id="cb1" />Bi-xenon glavna svjetla</label></li>';
									echo "<li><label><input type='checkbox' name='auto_duga_svjetla'"; if($row['auto_duga_svjetla'] == 1) echo "checked='checked'";  echo 'id="cb1" />Automatski rad dugih svjetala  </label></li>';
									echo "<li><label><input type='checkbox' name='led_straznja_svjetla'"; if($row['led_straznja_svjetla'] == 1) echo "checked='checked'";  echo 'id="cb1" />Stražnja svjetla sa LED diodama</label></li>';
									echo "<li><label><input type='checkbox' name='metalik_boja'"; if($row['metalik_boja'] == 1) echo "checked='checked'";  echo 'id="cb1" />Metalik boja</label></li>';
									echo "<li><label><input type='checkbox' name='krovni_nosaci'"; if($row['krovni_nosaci'] == 1) echo "checked='checked'";  echo 'id="cb1" />Krovni nosači</label></li>';
									echo "<li><label><input type='checkbox' name='nadzor_tlaka'"; if($row['nadzor_tlaka'] == 1) echo "checked='checked'";  echo 'id="cb1" />Nadzor tlaka u gumama</label></li>';

									echo "<li><label><input type='text' name='kotaci' size='2' maxlength='2' value='".$row['kotaci']."'  />\" lakometalni kotači</label></li>";
								?>



                                </ul >
								</div>

								<div style="clear:both;">

								<fieldset>
									<input class="submit-green" type="submit" value="Spremi promjene" />
								</fieldset>
								<input type="hidden" name="id" value="<?php echo $_GET['id']?>">
						</form>
								</fieldset>
							</div>
					</div>
                </div> <!-- End .module -->




			</div> <!-- End .grid_12 -->


            <div style="clear:both;"></div>


            <div style="clear:both;"></div>
        </div> <!-- End .container_12 -->

	</body>
</html>
