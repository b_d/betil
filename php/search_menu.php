<?php	
	function format_cijene($i)
	{
		$str = (string)$i;
		$return_str = '';
		if(strlen($str) == 4)
		{
			$return_str[0] = $str[0];
			$return_str[1] = '.';
			$return_str[2] = $str[1];
			$return_str[3] = $str[2];
			$return_str[4] = $str[3];
			return implode("",$return_str);
		}
		else if (strlen($str) == 5)
		{
			$return_str[0] = $str[0];
			$return_str[1] = $str[1];
			$return_str[2] = '.';
			$return_str[3] = $str[2];
			$return_str[4] = $str[3];
			$return_str[5] = $str[4];
			return implode("",$return_str);
		}
		else if (strlen($str) == 6)
		{
			$return_str[0] = $str[0];
			$return_str[1] = $str[1];
			$return_str[3] = $str[2];
			$return_str[2] = '.';
			$return_str[4] = $str[3];
			$return_str[5] = $str[4];
			$return_str[5] = $str[5];
			return implode("",$return_str);
		}
		else return 0;
	}
    
    if(isset($_GET['reset']))
    {
        include("config.php");
        connect();  
    }
    else if (!function_exists('connect')) 
    {
        include("php/config.php");
        connect();        
    }
    
	echo "<div class='searcher' id='searcher'>";
	
		echo "<div class='karoserija'>Karoserija:</br>
					<input type='checkbox' name='cbKaroserija' value='Gradski' onchange='SearchFormChange()' >Gradski<br>
					<input type='checkbox' name='cbKaroserija' value=\"Coupe', 'Kabriolet\" onchange='SearchFormChange()' >Coupe i cabrioleti</br>
					<input type='checkbox' name='cbKaroserija' value=\"Limuzina\" onchange='SearchFormChange()' >Limuzine</br>
					<input type='checkbox' name='cbKaroserija' value=\"SUV', 'Monovolumen', 'Karavan\" onchange='SearchFormChange()' >SUV, monovolumen i karavani</br>
					<input type='checkbox' name='cbKaroserija' value=\"4x4\" onchange='SearchFormChange()' >4x4</br>
					<input type='checkbox' name='cbKaroserija' value=\"Kombi i dostavna vozila', 'Putnička kombi vozila', 'Teretna vozila\" onchange='SearchFormChange()' >Kombi i dostavna vozila</br>
			</div>
		";	
		
	echo "<div class='marker'>
				Marke:
				<br>Ima ih: ";				
										$sql= mysql_query ("SELECT COUNT(id) as cnt FROM auto where aktivan = 1");
										while ($row = mysql_fetch_array($sql)){											
											echo "<label id='cntMarki'>(".$row['cnt'].")</label><br>";
										}
										/*
                                        na pocetku se pojavljuju samo marke koje imaju aktivne aute u bazi
										$sql= mysql_query ("select 
															 m.id_marka, m.marka,
															 count(a.marka) AS cnt 
															FROM marka_auta as m
															 inner join auto as a on a.marka = m.id_marka where a.aktivan = 1
															group by m.id_marka");
                                                            */

                                        $sql= mysql_query ("select 
															 m.id_marka, m.marka,
															(select count(*) from auto as a
                                                             where a.marka = m.id_marka and a.aktivan = 1
                                                            )                                                            
                                                            AS cnt 
															FROM marka_auta as m");



										while ($row = mysql_fetch_array($sql)){											
											echo "<input type='checkbox' name='cbMarke' value='".$row['id_marka']."' onchange='SearchFormChange()'";
                                            
                                            if($row['cnt'] >0)
                                                echo " checked";
                                            echo ">" .$row['marka']. " [".$row['cnt']."]<br>";											
										}
									
        echo "</div>";

		echo "<div class='goriva'>Vrsta goriva:</br>
                <input type='checkbox' name='cbGorivo' value='Benzin' onchange='SearchFormChange()' >Benzin</br>
                <input type='checkbox' name='cbGorivo' value='Benzin s plinom' onchange='SearchFormChange()' >Benzin s plinom<br>
                <input type='checkbox' name='cbGorivo' value='Diesel' onchange='SearchFormChange()' >Diesel</br>
            </div>"
        ;	
		
		echo "<div class='godine'>
                Godina prve registracije - od 
					<select id='select_godina_od' name='select_godina_od' class='input-short' onchange='SearchFormChange()'>
						<option value='0' >Odaberite godinu</option>";
						for($i = date("Y"); $i >= 1990 ; $i--) echo '<option value="'.$i.'">'.$i.'</option>';
						echo "</select>";
				echo "do 
					<select id='select_godina_do' name='select_godina_do' class='input-short'  onchange='SearchFormChange()'>
						<option value='0' selected='selected'>Odaberite godinu</option>";
						for($i = date("Y"); $i >= 1990 ; $i--) echo '<option value="'.$i.'">'.$i.'</option>';
				echo "</select>
              </div>";			
		
		
		echo "<div class='kilometri'>Kilometri - od 
					<select id='select_kilometri_od' name='select_kilometri_od' class='input-short' onchange='SearchFormChange()'>
						<option value='0' >Odaberite kilometre</option>
							<option value='30000'>30.000</option>		
							<option value='60000'>60.000</option>		
							<option value='90000'>90.000</option>		
							<option value='120000'>120.000</option>
							<option value='160000'>160.000</option>
							<option value='210000'>210.000</option>
						</select>";
						echo "Kilometri - do 
					<select id='select_kilometri_do' name='select_kilometri_do' class='input-short'  onchange='SearchFormChange()'>
						<option value='0' >Odaberite kilometre</option>
							<option value='30000'>30.000</option>		
							<option value='60000'>60.000</option>		
							<option value='90000'>90.000</option>		
							<option value='120000'>120.000</option>
							<option value='160000'>160.000</option>
							<option value='210000'>210.000</option>
					</select>
              </div>"
        ;
			
			
		echo "<div class='cijena'>Cijena - od 
					<select id='select_cijena_od' name='select_cijena_od' class='input-short' onchange='SearchFormChange()'>
						<option value='0' >Odaberite cijenu</option>";
						for($i = 1000; $i <= 50000 ; $i=$i+1000) echo '<option value="'.$i.'">'.format_cijene($i).' €</option>';
				echo "</select>";
                        
				echo "Cijena - do 
					<select id='select_cijena_do' name='select_cijena_do' class='input-short'  onchange='SearchFormChange()'>
						<option value='0' selected='selected'>Odaberite cijenu</option>";
						for($i = 1000; $i <= 50000 ; $i=$i+1000) echo '<option value="'.$i.'">'.format_cijene($i).' €</option>';
				echo "</select>
            </div>"
        ;				
		
		echo "<div class='sjedala'>+6 sjedala
					<input type='checkbox' name='cbSjedala' onchange='SearchFormChange()'><br>
				</div>
		";		
		echo "<div class='mjenjac'>Automatski mjenjac
					<input type='checkbox' name='cbMjenjac' onchange='SearchFormChange()'><br>
				</div>
		";		
		
		echo "<div class='btn_search' onclick='search();'>Traži!</div>";
		echo "<div class='btn_search' onclick='resetSearch();'>Reset tražilice</div>";
				
		
	echo "</div>";		
?>