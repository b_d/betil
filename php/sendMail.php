<?php
/**
 * This example shows sending a message using PHP's mail() function.
 */

require 'mailer.php';

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Set who the message is to be sent from
$mail->setFrom('betil@betil.com', 'Webmaster');
//Set who the message is to be sent to
$mail->addAddress('nodiizlagune@gmail.com');
//Set the subject line
$mail->Subject = 'Poruka sa Betil web-a';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
$emailAdresa = $_POST['email'];
$ime = $_POST['ime'];
$poruka = $_POST['upit'];
if(isset($_POST['telefon']))
   $telefon = $_POST['telefon'];
else
   $telefon = '/';
   


$mail->msgHTML('
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>PHPMailer Test</title>
</head>
<body>
<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;">
  <h1>Mail: </h1><p>'.$emailAdresa.'</p>
  </br><h1>Ime: </h1><p>'.$ime.'</p>
  </br><h1>Telefon: </h1><p>'.$telefon.'</p>
  </br><h1>Poruka: </h1><p>'.$poruka.'</p>
</div>
</body>
</html>');

//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}

header('Location: ../index.php');
?>