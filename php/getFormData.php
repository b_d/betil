<?php
	$godinaOd = 0; $godinaDo = 0; $cijenaOd = 0; $cijenaDo = 0; $kilometriDo = 0; $kilometriOd = 0;
	$dodatiWhere = true;
	$whereClause = "";
	$whereClauseMarke = "WHERE a.aktivan = 1 ";

	//marke
	if ($_GET['marke'][1] != ']'){
		$marke = json_decode(stripslashes($_GET['marke']));
		$markeUpit = '(';
		$poljeMarki = array();

		foreach($marke as $marka)
		{
			$markeUpit .= $marka . ",";
			$poljeMarki[] = $marka;
		}
		$markeUpit[strlen($markeUpit) - 1] = ')';
		$whereClauseMarke = "WHERE a.aktivan = 1 and a.marka IN " . $markeUpit ;
		$whereClause = $whereClauseMarke;
		$dodatiWhere = false;
	}

	//karoserija
	if ($_GET['karoserija'][1] != ']'){
		$karoserije = json_decode(stripslashes($_GET['karoserija']));
		$karoserijaUpit = '(';
		$poljeKaroserija = array();

		foreach($karoserije as $karoserija)
		{            
			$karoserijaUpit .= "'".$karoserija . "',";
			$poljeKaroserija[] = $karoserija;
		}
		$karoserijaUpit[strlen($karoserijaUpit) - 1] = ')';

		if($dodatiWhere == true ){
			$whereClause .= " WHERE a.aktivan=1 and a.karoserija IN " . $karoserijaUpit ;
			$dodatiWhere = false;
		}else
		{
			$whereClause .= " AND  a.karoserija IN " . $karoserijaUpit ;
		}
	}

	//gorivo
	if ($_GET['gorivo'][1] != ']'){
		$motori = json_decode(stripslashes($_GET['gorivo']));
		$motorUpit = '(';
		$poljeGoriva = array();

		foreach($motori as $motor)
		{
			$motorUpit .= "'".$motor . "',";
			$poljeGoriva[] = $motor;
		}
		$motorUpit[strlen($motorUpit) - 1] = ')';

		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.motor IN " . $motorUpit ;
			$dodatiWhere = false;
		}else
		{
			$whereClause .= " AND  a.motor IN " . $motorUpit ;
		}
	}

	// godine
	if ($_GET['godinaOd'] != 0 && $_GET['godinaDo'] != 0) 	{
		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.godina BETWEEN " . $_GET['godinaOd'] . " AND " . $_GET['godinaDo'] ;
			$dodatiWhere = false;
		}
		else
		{
			$whereClause .=  " AND a.godina BETWEEN " . $_GET['godinaOd'] . " AND " . $_GET['godinaDo'] ;
		}
		$godinaOd = $_GET['godinaOd'];
		$godinaDo = $_GET['godinaDo'];
	}else if ($_GET['godinaOd'] != 0 ){
		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.godina >= " . $_GET['godinaOd'] ;
			$dodatiWhere = false;
		}
		else
		{
			$whereClause .=  " AND a.godina >= " . $_GET['godinaOd'] ;
		}
		$godinaOd = $_GET['godinaOd'];
		$godinaDo = 0;

	}else if ($_GET['godinaDo'] != 0 ){
		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.godina <= " . $_GET['godinaDo'] ;
			$dodatiWhere = false;
		}
		else
		{
			$whereClause .=  " AND a.godina <= " . $_GET['godinaDo'] ;
		}
		$godinaOd = 0;
		$godinaDo = $_GET['godinaDo'];
	}


	//cijena

	if ($_GET['cijenaOd'] != 0 && $_GET['cijenaDo'] != 0) 	{
		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.cijena BETWEEN " . $_GET['cijenaOd'] . " AND " . $_GET['cijenaDo'] ;
			$dodatiWhere = false;
		}
		else
		{
			$whereClause .=  " AND a.cijena BETWEEN " . $_GET['cijenaOd'] . " AND " . $_GET['cijenaDo'] ;
		}
		$cijenaOd = $_GET['cijenaOd'];
		$cijenaDo = $_GET['cijenaDo'];
	}else if ($_GET['cijenaOd'] != 0 ){
		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.cijena >= " . $_GET['cijenaOd'] ;
			$dodatiWhere = false;
		}
		else
		{
			$whereClause .=  " AND a.cijena >= " . $_GET['cijenaOd'] ;
		}
		$cijenaOd = $_GET['cijenaOd'];
		$cijenaDo = 0;

	}else if ($_GET['cijenaDo'] != 0 ){
		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.cijena <= " . $_GET['cijenaDo'] ;
			$dodatiWhere = false;
		}
		else
		{
			$whereClause .=  " AND a.cijena <= " . $_GET['cijenaDo'] ;
		}
		$cijenaOd = 0;
		$cijenaDo = $_GET['cijenaDo'];
	}


	//kilometri

	if ($_GET['kilometriOd'] != 0 && $_GET['kilometriDo'] != 0) 	{
		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.kilometri BETWEEN " . $_GET['kilometriOd'] . " AND " . $_GET['kilometriDo'] ;
			$dodatiWhere = false;
		}
		else
		{
			$whereClause .=  " AND a.kilometri BETWEEN " . $_GET['kilometriOd'] . " AND " . $_GET['kilometriDo'] ;
		}
		$kilometriOd = $_GET['kilometriOd'];
		$kilometriDo = $_GET['kilometriDo'];
	}else if ($_GET['kilometriOd'] != 0 ){
		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.kilometri >= " . $_GET['kilometriOd'] ;
			$dodatiWhere = false;
		}
		else
		{
			$whereClause .=  " AND a.kilometri >= " . $_GET['kilometriOd'] ;
		}
		$kilometriOd = $_GET['kilometriOd'];
		$kilometriDo = 0;

	}else if ($_GET['kilometriDo'] != 0 ){
		if($dodatiWhere == true ){
			$whereClause .= " WHERE  a.aktivan=1 and a.kilometri <= " . $_GET['kilometriDo'] ;
			$dodatiWhere = false;
		}
		else
		{
			$whereClause .=  " AND a.kilometri <= " . $_GET['kilometriDo'] ;
		}
		$kilometriOd = 0;
		$kilometriDo = $_GET['kilometriDo'];
	}



	// sjedala
	if ($_GET['sjedala'][1] != ']'){

		$sjedala = 1;
		if($dodatiWhere == true){
			$whereClause .= " WHERE  a.aktivan=1 and a.sjedala >= 6";
			$dodatiWhere = false;
		}
		else{
			$whereClause .= " AND a.sjedala >= 6";
		}
	}
	else
	{
		$sjedala = 0;
	}

	// mjenjac
	if ($_GET['mjenjac'][1] != ']'){

		$mjenjac = 1;
		if($dodatiWhere == true){
			$whereClause .= " WHERE  a.aktivan=1 and a.mjenjac = 'Automatski mjenjač'";
			$dodatiWhere = false;
		}
		else{
			$whereClause .= " AND a.mjenjac = 'Automatski mjenjač'";
		}
	}
	else
	{
		$mjenjac = 0;
	}
	/*
	if (strpos($whereClause,'WHERE a.marka IN') !== false) {
		$whereClauseBezMarke = preg_match('/WHERE a.marka IN/',$a);
	}
	 */
?>
